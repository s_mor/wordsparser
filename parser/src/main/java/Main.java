public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No arguments! Please run with filepath");
            return;
        }
        String ipAddress = System.getProperty("ipAddress");
        String port = System.getProperty("port");
        String dbName = System.getProperty("dbName");
        String username = System.getProperty("username");
        String pass = System.getProperty("pass");
        try {
            WordsRepository repository = new WordsRepository(ipAddress, port, dbName, username, pass);
            WordsParser wordsParser = new WordsParser(repository);
            wordsParser.readAndParse(args[0]);
        }
        catch (RuntimeException e)
        {
            System.err.println(e.getMessage());
        }
    }
}
