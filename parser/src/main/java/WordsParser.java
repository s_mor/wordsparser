import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordsParser {
    private static final String PATTERN = "[^A-Za-zА-Яа-я0-9]+";
    private WordsRepository repository = null;

    public WordsParser(WordsRepository repository) {
        this.repository = repository;
    }

    public void readAndParse(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            scanner.useDelimiter(PATTERN);
            while (scanner.hasNext()) {
                String next = scanner.next();
                if (!repository.isExist(next)) {
                    repository.createNewWord(next);
                } else {
                    repository.updateCount(next);
                }

            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File not found",e);
        }
    }
}
