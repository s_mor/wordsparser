import java.sql.*;

public class WordsRepository {
    private String dbUrl;
    private String username;
    private String pass;

    public WordsRepository(String ipAddress, String port, String dbName, String username, String pass) {
        this.dbUrl = "jdbc:postgresql://" + ipAddress + ":" + port + "/" + dbName;
        this.username = username;
        this.pass = pass;
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(dbUrl, username, pass);
        } catch (SQLException e) {
            throw new RuntimeException("DB connection error",e);
        }
    }

    public boolean isExist(String word) {
        try (Connection connect = connect();
             Statement st = connect.createStatement();
             ResultSet rs = st.executeQuery("SELECT * FROM \"WORDS\" WHERE \"WORD\"='" + word + "'")) {
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Query execution error",e);
        }
        return false;
    }

    public void createNewWord(String word) {
        try (Connection connect = connect();
             Statement st = connect.createStatement()) {
            st.execute("INSERT INTO \"WORDS\" VALUES ('" + word + "',1)");
        } catch (SQLException e) {
            throw new RuntimeException("Query execution error",e);
        }
    }

    public void updateCount(String word) {
        try (Connection connect = connect();
             Statement st = connect.createStatement()) {
            st.execute("UPDATE \"WORDS\" SET \"WORD_COUNT\"=\"WORD_COUNT\"+1 WHERE \"WORD\"='" + word + "'");
        } catch (SQLException e) {
            throw new RuntimeException("Query execution error",e);
        }

    }

}
